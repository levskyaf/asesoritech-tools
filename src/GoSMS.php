<?php

namespace Asesoritech\Tools;
use Illuminate\Support\Facades\Http;

class GoSMS
{
    function __construct(){
        $this->apiKey = env("ASESORITECH_API_KEY");
        $this->apiUrl = "https://asesoritech.com.do/api/sms/";
    }

    function status(){
        return Http::withToken($this->apiKey)->get($this->apiUrl."status")->json();
    }

    function sendSms($phoneNumber,$message){
        return Http::withToken($this->apiKey)->post($this->apiUrl."sendMessage", [
            'phoneNumber' => $phoneNumber,
            'message'     => base64_encode($message),
        ])->json();
    }
}