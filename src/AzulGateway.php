<?php
namespace Asesoritech\Tools;

class AzulGateway
{
    function __construct(){
        $this->merchantID   = env("AZUL_MERCHANT_ID");
        $this->merchantName = env("AZUL_MERCHANT_NAME");
        $this->merchantType = env("AZUL_MERCHANT_TYPE");
        $this->merchantLogo = env("AZUL_MERCHANT_LOGO");
        $this->currencyCode = env("AZUL_CURRENCY_CODE","$");
        $this->approvedURL  = env("AZUL_APPROVED_URL",url('azul/approved'));
        $this->declinedURL  = env("AZUL_DECLINED_URL",url('azul/declined'));
        $this->cancelURL    = env("AZUL_CANCEL_URL",url('azul/cancelled'));
        $this->ITBIS        = env("AZUL_ITBIS","000");
        $this->authKey      = "asdhakjshdkjasdasmndajksdkjaskldga8odya9d8yoasyd98asdyaisdhoaisyd0a8sydoashd8oasydoiahdpiashd09ayusidhaos8dy0a8dya08syd0a8ssdsax";
        $this->url          = $this->gatewayUrl();
    }

    function createPaymentData($id,$amount,$image) : array{
        $this->data                    = $this->requestData();
        $this->data["OrderNumber"]     = (string)$id;
        $this->data["Amount"]          = (string)$amount."00";
        $this->data["AuthHash"]        = $this->createAuthHash();
        $this->data["DesignV2"]        = "1";
        $this->data["LogoImageUrl"]    = $this->merchantLogo;
        $this->data["ProductImageUrl"] = $image;
        return $this->data;
    }

    function invokePaymentPage($id,$amount,$image){
        $html   = "<form action='{$this->url}' method='post' id = 'azul'>";
        $fields = $this->createPaymentData($id,$amount,$image);
        foreach ($fields as $name => $value){
            $html.="<input type='hidden' name='{$name}' value='{$value}'>";
        }
        $html.="</form>";
        $html.="<script>document.forms['azul'].submit()</script>";
        return $html;
    }

    private function gatewayUrl() : string{
        if(\App::environment() === 'production'){
            return "https://pagos.azul.com.do/paymentpage/Default.aspx";
        }

        if(\App::environment() === 'local'){
            return "https://pruebas.azul.com.do/paymentpage/Default.aspx";
        }
    }

    private function requestData() : array{
        return [
            "MerchantId"         => $this->merchantID,
            "MerchantName"       => $this->merchantName,
            "MerchantType"       => $this->merchantType,
            "CurrencyCode"       => $this->currencyCode,
            "OrderNumber"        => null,
            "Amount"             => null,
            "ITBIS"              => $this->ITBIS,
            "ApprovedUrl"        => $this->approvedURL,
            "DeclinedUrl"        => $this->declinedURL,
            "CancelUrl"          => $this->cancelURL,
            "UseCustomField1"    => "0",
            "CustomField1Label"  => "null",
            "CustomField1Value"  => "null",
            "UseCustomField2"    => "0",
            "CustomField2Label"  => "null",
            "CustomField2Value"  => "null",
        ];
    }

    private function createAuthHash() : string{
        $keyChain          = $this->createKeyChain();
        $response_string   = mb_convert_encoding($keyChain, 'UTF-16LE', 'ASCII');
        $hash              = hash_hmac('sha512',$keyChain,$this->authKey);
        return $hash;
    }

    private function createKeyChain() : string{
        $keyChain = "";
        foreach($this->data as $key => $data){
            $keyChain .= $data;
        }
        $keyChain .= $this->authKey;
        return $keyChain;
    }
}