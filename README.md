


# Asesoritech Tools

### Easy Implementations of asesoritech services for laravel applications

## Installation

1. Install packages dependencies

```
composer require asesoritech/tools
```
  
# SMS 
 Install guzzle dependencies
```
composer require guzzlehttp/guzzle
```
### Set up env variables
Set the api key provided
```
ASESORITECH_API_KEY=""

```

## Usage
#### Send Message

    $goSMS  = new GoSMS();
    $sent   = $goSMS->sendMessage("8090000000","Exmple message");
    echo $sent;
####  Check line status
  

    $goSMS  = new GoSMS();
    $status = $goSMS->status();
    echo $status;

# Azul Payment Page

### Set up env variables

```
AZUL_MERCHANT_ID="Identification of the assigned business"
AZUL_MERCHANT_NAME="Business name"
AZUL_MERCHANT_TYPE="Business type information"
AZUL_MERCHANT_LOGO="Business logo image full url"

** Optional **
AZUL_APPROVED_URL Default: Base Url + 'azul/approved' 
AZUL_DECLINED_URL Default: Base Url + 'azul/declined' 
AZUL_CANCEL_URL   Default: Base Url + 'azul/cancelled' 
```
## Note
If **(APP_ENV = local)** the Azul test environment will be set, **otherwise** the production domain and configuration will be set

## Usage
### Create payment data
Create an array with all (Azul) required fields including AuthHash

     $azulGateway = new AzulGateway();
     $data = $azulGateway->createPaymentData($orderId,$amount,$productImageUrl);
     dump($data);
### Invoke payment page
Automatically create and submit HTML form with all required fields and data to redirect to payment page;


     $azulGateway = new AzulGateway();
     $htmlForm    = $azulGateway->invokePaymentPage($orderId,$amount,$productImageUrl);
     echo $htmlForm;



